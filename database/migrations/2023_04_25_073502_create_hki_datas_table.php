<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHkiDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hki_datas', function (Blueprint $table) {
            $table->id();
            $table->string('students_id');
            $table->string('hki_types_id');
            $table->string('title');
            $table->year('year');
            $table->string('main_mentor');
            $table->string('companion_mentor');
            $table->string('chief_examiner');
            $table->string('examiner_1');
            $table->string('examiner_2');
            $table->string('examiner_3');
            $table->string('file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hki_datas');
    }
}
